# Golem shiny

# Run as Docker
To build and run this is as docker app, please run the following commands from the root folder of this repo:

- `docker build -t golemtestapp .`
- `docker run -p 3838:3838 golemtestapp`
